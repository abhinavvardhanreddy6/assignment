fn main() {
    let table_number = 10;
    
    println!("Division Table for {}", table_number);
    
    for i in 1..=10 {
        let result = table_number / i;
        println!("{}/ {} = {}", table_number, i, result);
    }
}

fn main() {
    println!("Even numbers:");
    for num in 1..=1000 {
        if num % 2 == 0 {
            println!("{}", num);
        }
    }

    println!("Odd numbers:");
    for num in 1..=1000 {
        if num % 2 != 0 {
            println!("{}", num);
        }
    }
}


fn main() {
    let name = "Jampana Vikas Varma";
    let branch = "Computer Science(AI&ML)";
    let year = 2023;
    let roll_no = "20B61A6619";
    let college_name = "Nalla Malla Reddy Engineering College";

    println!("Name: {}", name);
    println!("Branch: {}", branch);
    println!("Year: {}", year);
    println!("Roll No: {}", roll_no);
    println!("College Name: {}", college_name);
}

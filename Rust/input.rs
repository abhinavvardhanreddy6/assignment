use std::io;

fn main() {
    let mut input1 = String::new();
    let mut input2 = String::new();

    println!("Enter two numbers: ");

    io::stdin().read_line(&mut input1).unwrap();
    io::stdin().read_line(&mut input2).unwrap();

    let number1: f64 = input1.trim().parse().unwrap();
    let number2: f64 = input2.trim().parse().unwrap();

    println!("Addition: {} + {} = {}", number1, number2, number1 + number2);
    println!("Subtraction: {} - {} = {}", number1, number2, number1 - number2);
    println!("Multiplication: {} * {} = {}", number1, number2, number1 * number2);
    println!("Division: {} / {} = {}", number1, number2, number1 / number2);
}
